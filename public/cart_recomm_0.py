# -*- coding: utf-8 -*-
"""
根據免運門檻，推薦同一個供應商其他商品

@author: brenda_liu
"""

import pandas as pd
from io import StringIO
import pyodide_js

csv_data = pyodide_js.globals.get('csv_data')
cart_pid = pyodide_js.globals.get('cart_pid')
current_amount = pyodide_js.globals.get('current_amount')
supplier_id = pyodide_js.globals.get('supplier_id')
ship_free = pyodide_js.globals.get('ship_free')

##
need_amount = ship_free - current_amount

# 讀取CSV
df = pd.read_csv(StringIO(csv_data))

# 使用傳入的參數做運算
# 找出同供應商
df = df[(df['supplier_id']==supplier_id) & (~df.pid.isin(cart_pid)) & (df.is_stock==1)].sort_values(by='ek',ascending=False).reset_index(drop=True)
df = df[df.member_price < need_amount]

# 將結果輸出JSON
result = df.head(10).pid.tolist()
result = ",".join([str(int(p)) for p in result])  #一定要是文字

# 設置結果為全局變數，讓外面讀取
pyodide_js.globals['result'] = result