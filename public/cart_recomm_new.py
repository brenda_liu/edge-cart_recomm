# -*- coding: utf-8 -*-
"""
根據免運門檻，推薦同一個供應商其他商品

@author: brenda_liu
"""
import asyncio
import pandas as pd
import json
from pyodide.http import pyfetch
import pyodide_js

async def post_request(url, data):
    response = await pyfetch(
        url,
        method="POST",
        headers={"Content-Type": "application/json"},
        body=json.dumps(data),
        credentials="include"
    )
    return await response.json()

async def main():
    # 從 JavaScript 環境獲取變量
    user_pseudo_id = pyodide_js.globals.get('user_pseudo_id')
    cart_pid = pyodide_js.globals.get('cart_pid')
    current_amount = pyodide_js.globals.get('current_amount')
    supplier_id = pyodide_js.globals.get('supplier_id')
    ship_free = pyodide_js.globals.get('ship_free')

    need_amount = ship_free - current_amount + 50 #超過一點點沒關係
    print(f'need_amound = {need_amount}')

    # 使用傳入的參數做運算
    api_paras = {
        "target_value": user_pseudo_id, 
        "q1_x": 0.5, 
        "supplier_y": 1,
        "filter": { "k": "10000", "v": [supplier_id,"","","",""] },
        "list_num": 500,
        "type": 3
    }

    result = await post_request('https://k8aiapi.shopping.friday.tw/api/getalist', api_paras)
    df_1 = pd.DataFrame(result[0]['pids'])
    df_1 = df_1[['pid','ek']]
    pid_list = df_1.pid.tolist()
    print(f'getalist OK! {len(df_1)}')
    
    df = df_1.copy()
    df['price'] = [random.randint(10, 1000) for _ in range(len(df))] # 隨便給個金額(不能打http)

    # 找出同供應商
    df = df[(~df.pid.isin(cart_pid))].sort_values(by='ek',ascending=False).reset_index(drop=True)
    df = df[df.price <= need_amount]
    # 將結果輸出JSON
    result = df.head(10).pid.tolist()
    result = ",".join([str(int(p)) for p in result])  #一定要是文字

    # 設置結果為全局變數，讓外面讀取
    pyodide_js.globals.set('result', result)

# 運行主函數
await main()